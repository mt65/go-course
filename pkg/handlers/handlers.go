package handlers

import (
	"gitlab.com/mt65/go-course/pkg/config"
	"gitlab.com/mt65/go-course/pkg/models"
	"gitlab.com/mt65/go-course/pkg/render"
	"net/http"
)

// Repo holds the repository
var Repo *Repository

// Repository type for repository
type Repository struct {
	App *config.AppConfig
}

// NewRepo creates new repository
func NewRepo(a *config.AppConfig) *Repository {
	return &Repository{
		App: a,
	}
}

// NewHandlers sets Repo for handlers package
func NewHandlers(r *Repository) {
	Repo = r
}

// Home is a home page handler
func (rep *Repository) Home(w http.ResponseWriter, r *http.Request) {
	stringMap := make(map[string]string)
	stringMap["test"] = "This is the passed data"
	remoteIP := rep.App.Session.GetString(r.Context(), "remote_ip")
	stringMap["remote_ip"] = remoteIP
	render.RenderTemplate(w, "home.page.tmpl", &models.TemplateData{
		StringMap: stringMap,
	})
}

// About is an about page handler
func (rep *Repository) About(w http.ResponseWriter, r *http.Request) {
	remoteIP := r.RemoteAddr
	rep.App.Session.Put(r.Context(), "remote_ip", remoteIP)
	render.RenderTemplate(w, "about.page.tmpl", &models.TemplateData{})
}
