package models

// TemplateData holds the data to pass from handlers to templates
type TemplateData struct {
	StringMap map[string]string
	IntMap    map[string]int
	FloatMap  map[string]float32
	Data      map[string]interface{}
	SCRFToken string
	Flash     string
	Warning   string
	Error     string
}
