package render

import (
	"bytes"
	"fmt"
	"gitlab.com/mt65/go-course/pkg/config"
	"gitlab.com/mt65/go-course/pkg/models"
	"html/template"
	"log"
	"net/http"
	"path/filepath"
)

var app *config.AppConfig

// NewTemplates sets the app config for render package
func NewTemplates(a *config.AppConfig) {
	app = a
}

// AddDefaultTemplateData adds default data to every template
func AddDefaultTemplateData(td *models.TemplateData) *models.TemplateData {
	return td
}

// RenderTemplate renders given template using html/template package
func RenderTemplate(w http.ResponseWriter, tmpl string, td *models.TemplateData) {
	var tc map[string]*template.Template

	if app.UseCache {
		tc = app.TemplateCache
	} else {
		tc, _ = CreateTemplateCache()
	}

	t, ok := tc[tmpl]
	if !ok {
		log.Fatal(fmt.Sprintf("Can't find template %s in the cache", tmpl))
	}

	td = AddDefaultTemplateData(td)

	buff := new(bytes.Buffer)
	_ = t.Execute(buff, td)

	_, err := buff.WriteTo(w)
	if err != nil {
		fmt.Println("Error writing template to browser", err)
	}
}

func CreateTemplateCache() (map[string]*template.Template, error) {
	myCache := map[string]*template.Template{}
	baseTemplatesPath := "./../../templates/"

	pages, err := filepath.Glob(baseTemplatesPath + "*.page.tmpl")
	if err != nil {
		return myCache, err
	}

	for _, page := range pages {
		name := filepath.Base(page)

		ts, err := template.New(name).ParseFiles(page)
		if err != nil {
			return myCache, err
		}

		layouts, err := filepath.Glob(baseTemplatesPath + "*.layout.tmpl")

		if len(layouts) > 0 {
			ts, err = ts.ParseGlob(baseTemplatesPath + "*.layout.tmpl")
			if err != nil {
				return myCache, err
			}
		}

		myCache[name] = ts
	}

	return myCache, nil
}
