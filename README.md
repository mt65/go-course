# Bookings and Reservations

This is the repository for Bookings and Reservations project for learning Go.

- Built in Go version 1.20
- Uses the [chi router](https://github.com/go-chi/chi)
- Uses [Alex Edwards SCS](https://github.com/alexedwards/scs) session management
- Uses [nosurf](https://github.com/justinas/nosurf)